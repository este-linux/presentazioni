# ServiziLiberi

Presentazione di [ServiziLiberi](https://serviziliberi.it), per promuovere il software libero nelle scuole di informatica, per dare spunti agli studenti su opportunità nel mondo del lavoro, basandosi sulla filosofia del software libero.

Creata utilizzando [Reveal.js](https://revealjs.com/).

## Installazione

Occorre scaricare [reveal.js](https://github.com/hakimel/reveal.js) come submodulo e avviarlo impostando come cartella di lavoro la cartella attuale:

```bash
git clone https://codeberg.org/este-linux/presentazioni.git
cd presentazioni && cd "2024-02-03 ServiziLiberi per scuole"
git submodule add https://github.com/hakimel/reveal.js.git 
cd reveal.js && npm install && cd ..
```

## Avvio

Una voltra preparato l'ambiente, è possibile avviare un server web temporaneo per servire il file `index.html` con:

```bash
python3 -m http.server 8000
```

Al termine, la presentazione sarà visualizzabile all'indirizzo http://localhost:8000
