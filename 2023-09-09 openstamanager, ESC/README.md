# OpenSTAManager

Gestionale libero nato nel lontano 2008 come hobby e diventato un gestionale molto completo per aziende e professionisti. E' mantenuto oggi da un'azienda di 6 persone, completamente open source. Presentazione di come funziona il ramo 2.4 attuale, presentazione del ramo 2.5 (riscrittura con Laravel) e anticipazione di un ulteriore fork di una versione 3.0 con Mithril.js.

Creata utilizzando [Reveal.js](https://revealjs.com/).

## Installazione

Occorre scaricare [reveal.js](https://github.com/hakimel/reveal.js) come submodulo e avviarlo impostando come cartella di lavoro la cartella attuale:

```bash
git submodule update --init reveal.js
cd reveal.js && npm install && cd ..
```

## Avvio

Una voltra preparato l'ambiente, è possibile avviare un server web temporaneo per servire il file `index.html` con:

```bash
python3 -m http.server 8000
```

Al termine, la presentazione sarà visualizzabile all'indirizzo http://localhost:8000
