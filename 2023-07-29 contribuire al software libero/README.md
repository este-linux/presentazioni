# Contribuire al software libero

Presentazione che spiega come contribuire al software libero.

Creata utilizzando [Reveal.js](https://revealjs.com/).

## Installazione

Occorre scaricare [reveal.js](https://github.com/hakimel/reveal.js) come submodulo e avviarlo impostando come cartella di lavoro la cartella attuale:

```bash
git submodule add https://github.com/hakimel/reveal.js.git
cd reveal.js && npm install && cd ..
```

## Avvio

Una voltra preparato l'ambiente, è possibile avviare un server web temporaneo per servire il file `index.html` con:

```bash
python3 -m http.server 8000
```

Al termine, la presentazione sarà visualizzabile all'indirizzo http://localhost:8000
